# Web-UI-API-DB

A four container architecture pattern for structuring and configuring modern web applications.

A collection of template files for getting a project up and running.

This pattern leverages Docker containers and docker-compose. If you're new to containers or need to get the software installed, [maybe give this guide a try](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04).

## Additional documentation

    ui/content/\*

## Setup

Clone locally

    cd path/to/project_name_parent_dir/
    git clone https://gitlab.com/charlesbrandt/web-ui-api-db project_name

To see what the remote server is set to, use:

    git remote -v

    git remote add upstream https://gitlab.com/charlesbrandt/web-ui-api-db

Or

    git remote add upstream git@gitlab.com:charlesbrandt/web-ui-api-db.git

Update origin to be your own project repo

    git remote remove origin

    git remote add origin ssh://user@server:/GitRepos/myproject.git

Open this file in an editor or IDE of your choice:

    edit README.md

Find and replace all instances of `boilerplate` with `project_name`. (Manually or with a script)

    find . -type f -exec sed -i 's/boilerplate/project_name/g' {} +

    grep -ir boilerplate *

[via](https://unix.stackexchange.com/questions/112023/how-can-i-replace-a-string-in-a-files/112024#112024)

Replace this README.md file with a description of your own project.

## Next Steps

[Infrastructure configuration (Dev-Ops)](docker-README.md)

Set up the [front-end client ui](ui/README-ui.md)
