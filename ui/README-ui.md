# boilerplate

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


## UI Browser-based Client

A front-end (web-based) application. UI stands for 'User Interface'. 

What gets delivered to the client / browser for the person using the service. 

Javascirpt (JS) based because Javascript is the language of the web. 


## Javascript Front-end Frameworks

There are many types of frameworks within the Node ecosystem. 

No preference? Check out Nuxt:

https://zendev.com/2018/09/17/frontend-architecture-lessons-from-nuxt-js.html


## Nuxt

Be sure you've run through [docker-README.md](../docker-README.md) first...

[Cannot use `create-nuxt-app` under docker at the moment](https://github.com/nuxt/create-nuxt-app/issues/383)

Use system node to generate the nuxt-app

    npx create-nuxt-app boilerplate

https://gitlab.com/charlesbrandt/public/-/blob/master/code/javascript/nuxt.md
https://gitlab.com/charlesbrandt/public/

Move the code to the right place, including '.' files. Remove `node_modules`

``` bash
rm -r boilerplate/node_modules/
mv boilerplate/* .
mv boilerplate/.* .
rm -r .git
```

and run:

    npm install 

edit package.json:

nuxt command gets removed after container restarts. 
way to run equivalent without nuxt command
```
"scripts": {
  "dev": "nuxt --hostname 0.0.0.0",
}
```


from there, run

    npm run dev

## Browser

See if it works! :)

https://localhost:8888/

