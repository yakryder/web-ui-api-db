# Architecture

Four containers for main services of:

  - web
  - ui
  - api
  - db

Anything can be used within them
as long as it generally conforms to the layers. 

Popular options for different layers... 

## Web Server

Nginx is a popular web server used to:

  - Serve static files
  - Serve generated content created by the front-end client node server
  - Proxy calls to interactive front-end or back-end api node servers

## User Interface / Application Programming Interface

These two get a little blurry sometimes. 

On the UI, you only have one option:

  - Javascript / JS / Node

You can use Node on the API side too. 

If you know other technology stacks for the back-end server side, (e.g. Python or Elixir)

From here you get into frameworks and libraries, which is beyond the scope of this project. 

## Database

Mongo is a popular option these days. Using it as a default here.

Use whatever suits your needs. 


## Core technologies

Linux containers. Docker is assumed.

It may be possible to port this idea to other container solutions. 


## Operations / Running / Installation

[Operations](../docker-README.md)
