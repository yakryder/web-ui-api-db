## Style / Sass / CSS

Nuxt includes Vue Loader out of the box. To get Sass working,

    npm install --save-dev node-sass sass-loader

https://nuxtjs.org/faq/pre-processors/

To get, for example, bulma utilities available in every component
(don't need nuxtjs/bulma in this case)

    npm install --save bulma

create `ui/assets/main.scss`

```
/* Import Bulma Utilities */
@import '~bulma/sass/utilities/initial-variables';
@import '~bulma/sass/utilities/functions';

/* Variable changes go here */

// Colours
$primary: #d0eeed;
$secondary: #d0e0ee;

// Breakpoints
$tablet: 724px;
$desktop: 960px + (2 * $gap);
$widescreen: 1152px + (2 * $gap);
$fullhd: 1344px + (2 * $gap);

/* Import the rest of Bulma */
@import '~bulma';
```

in nuxt.config.js

```
css: [{ src: '~/assets/main.scss', lang: 'scss' }],
```

Then in components, bulma variables should be accessible

```
<style scoped lang="scss">
.responsive-logo {
  border: 1px dashed #990000;
  @include until($desktop) {
    border-bottom: 1px solid green;
    width: '112';
    height: '28';
  }

  @include from($tablet) {
    border-left: 2px solid yellow;
  }

  @include tablet {
    border-top: 2px solid orange;
  }
  @include widescreen-only {
    border-right: 2px solid blue;
  }
}
</style>
```

npm install --save @nuxtjs/style-resources

another guide
https://www.freecodecamp.org/news/up-goind-with-nuxt-js-bulma-and-sass/

This guide seems better... may allow skipping style-resources?
https://www.gavsblog.com/blog/adding-bulma-to-nuxt-js-with-changeable-variables

seems like style-resources is necessary for this functionality

https://nuxtjs.org/api/configuration-css/

### Themes

For Vuetify, themes are defined in `nuxt.config.js`

and custom variables can be placed:

    assets/variables.scss
