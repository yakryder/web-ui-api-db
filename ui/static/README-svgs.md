# SVGs

SVGs can be generated from PDF or other vector based formats if not available directly.

## Conversion Process

 - download
 - extract (.zip)
 - imported pdf to inkscape
 - save as svg
 - optimize with https://jakearchibald.github.io/svgomg/

