# boilerplate-api

> The back-end server.

## About

This is typically where application actions are defined. Things like authentication, authorization, and persistence or storage usually happen here. 

By default, the boilerplate is [configured to use a Node JS based api server](../docker-compose.yml). 

You could just as easily substitute Python or any other language that you prefer. 

## Node API Frameworks

There are many types of frameworks within the Node ecosystem. 

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

https://docs.feathersjs.com/guides/basics/starting.html


## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/boilerplate-api
    npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```


## Configuration Reminder

Make sure that the API is listening on 0.0.0.0 *within the container*, not just localhost. This allows other containers on the docker network to access the service. If you're forwarding traffic through nginx to the API, nginx would need to be able to talk to the API server over the docker network. 

For example, with express, configure with:

```
exports.express = {
  host: "0.0.0.0",
  port: 12345,

```

## Access

after `npm install` and `node index.js` in the container, should be able to go:

https://localhost:8888/api/people


## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

