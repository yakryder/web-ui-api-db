# Web UI API DB in Docker

https://gitlab.com/charlesbrandt/web-ui-api-db

A generalized approach for containerizing web applications.

The architecture provides four primary layers:

- web - static server
- ui - frontend client
- api - backend server
- db - persistence storage

If you have a preferred technology for any given layer, it's easy to replace the default option with your choice.

[More details on the architecture](docs/architecture.md)

## Setup

Create shared volumes to ensure container modules are not confused with host level modules (if you run the same services at the host level):

    docker volume create --name=boilerplate_ui_modules
    docker volume create --name=boilerplate_api_modules

Generate SSL keys with:

    mkdir -p web/ssl
    openssl req -subj '/CN=localhost' -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./web/ssl/nginx.key -out ./web/ssl/nginx.crt

Install node packages for the UI and API with

    docker-compose -p boilerplate up -d

    # or just...
    docker-compose up -d

Connect to the containers

    docker-compose -p boilerplate exec ui bash

and run

    npm install

The same approach allows running the UI/API build step interactively. Running interactively is useful during development and debugging cycles.

## Starting / Stopping

Bring up the containers:

    docker-compose up -d

Make sure everything is running (no Exit 1 States):

    docker-compose ps

To shut down the containers:

    docker-compose down -v

## Configuration

Try it out how it is.

The default configuration should be enough to get up and running.

To make adjustments, edit and review [docker-compose.yml](docker-compose.yml).

You can check which ports are available locally and find something unique.

    netstat -panl | grep " LISTEN "

Comment out containers that you don't need right away.

Update the `config/nginx.conf` as needed, especially to proxy requests to the right place on the API server.

To reload nginx without reloading all of the containers:

    docker-compose -p boilerplate exec web nginx -s reload

## Docker-compose

### -p

By default, the project name is the parent directory name. This is usually the case for local development setups.

There are times, however, when it is useful to make directory names that are different than the project name. For example, working on a different branch, it may be easier to use the branch name instead of the project name for the parent directory.

If the parent directory is **not** equal to the project name, you'll want to pass the project name in to all of the above docker-compose commands.

Using `-p boilerplate` allows the project name (and consequently the container name) to be consistent from one deployment to the next. That way containers are named with `boilerplate_[service]_1`.

This allows configuration files to be written ahead of time to work.

If you've checked out the repository to a directory named `boilerplate`, the project name `boilerplate` will be assumed by docker and the `-p` option may be omitted.

### -f

If you have a compose file named something other than `docker-compose.yml`, you can specify the name with a `-f` flag:

    docker-compose -p boilerplate -f docker-compose-prod.yml up -d

## Troubleshooting

Most containers have `curl` available. Connect to one and then try making requests to the service you're having issue with.

    docker-compose -p boilerplate exec web bash
    curl -X GET http://boilerplate_api_1:3030/

(we don't need the `/api` suffix since we're behind the nginx proxy that normally adds `/api` for us)

Also, you can always insert `console.log()` statements in the code to see what values are at any given point.
